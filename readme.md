# EXO_encounter 667

Install [LÖVE](https://love2d.org) version 11.1 and run `love .` in
this directory.

Downloads for Windows, Macs and GNU/Linux are [available on itch.io](https://technomancy.itch.io/exo-encounter-667).

Press `escape` in the game to see a list of key bindings.

By [Dan Larkin](https://danlarkin.org/) and [Phil Hagelberg](https://technomancy.us).

## Licenses

Original code, prose, map, and images copyright © 2018 Dan Larkin, Phil
Hagelberg, Zach Hagelberg, and Noah Hagelberg.

Distributed under the GNU General Public License version 3 or later; see file license.txt.

Licensing of third-party art and libraries described in [credits](credits.md)
